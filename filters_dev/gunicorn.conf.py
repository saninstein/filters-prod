bind = '127.0.0.1:8000'
workers = 3
errorlog = '/filter/logs/gunicorn_error.log'
loglevel = 'error'
accesslog = '/filter/logs/gunicorn_access.log'
user = 'nobody'

