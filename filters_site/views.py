from django.shortcuts import render_to_response,  redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template.loader import get_template
from django.http import HttpResponse
from django.core.context_processors import csrf
from filters_site.forms import MessageForm
from filters_site.models import *


def is_eng(req):
    if req.session.get('eng', False):
        return True
    return False


def skill(req, num=''):
    template = 'index.html'
    if is_eng(req):
        template = 'index_eng.html'
    args = dict()
    try:
        args['item'] = Skills.objects.get(id=num)
    except Skills.DoesNotExist:
        args['item'] = Skills.objects.filter()[:1]
    contact = Contacts.objects.all()
    args['contacts'] = {'tel1': contact.get(pk=1), 'tel2': contact.get(pk=2), 'email': contact.get(pk=3)}
    articles = Article.objects.only(
        'name',
        'name_en'
    ).order_by('-id')
    args['articles'] = articles[:3]
    return render_to_response("article/" + template, args)


def slide(req, num=''):
    template = 'index.html'
    if is_eng(req):
        template = 'index_eng.html'
    args = dict()
    try:
        args['item'] = Slider.objects.get(id=num)
    except Slider.DoesNotExist:
        args['item'] = Slider.objects.filter()[:1]
    contact = Contacts.objects.all()
    args['contacts'] = {'tel1': contact.get(pk=1), 'tel2': contact.get(pk=2), 'email': contact.get(pk=3)}
    articles = Article.objects.only(
        'name',
        'name_en'
    ).order_by('-id')
    args['articles'] = articles[:3]
    return render_to_response("article/" + template, args)


def general(req):
    template = 'index.html'
    if is_eng(req):
        template = 'index_eng.html'
    args = dict()
    args['items'] = Skills.objects.only(
        'name',
        'name_en',
        'short_description',
        'short_description_en',
        'photo'
    )[:3]
    contact = Contacts.objects.all()
    args['contacts'] = {'tel1': contact.get(pk=1), 'tel2': contact.get(pk=2), 'email': contact.get(pk=3)}
    args['slides'] = Slider.objects.all()
    articles = Article.objects.only(
        'name',
        'name_en'
    ).order_by('-id')
    args['articles'] = articles[:3]
    return render_to_response("general/" + template, args)


def contacts(req):
    template = 'index.html'
    if is_eng(req):
        template = 'index_eng.html'
    args = dict()
    articles = Article.objects.only(
        'name',
        'name_en'
    ).order_by('-id')
    args['articles'] = articles[:3]
    contact = Contacts.objects.all()
    try:
        args['contacts'] = {
            'tel1': contact.get(pk=1),
            'tel2': contact.get(pk=2),
            'email': contact.get(pk=3),
            'country': contact.get(pk=4),
            'town': contact.get(pk=5),
            'place': contact.get(pk=6),
            'country_en': contact.get(pk=7),
            'town_en': contact.get(pk=8),
            'place_en': contact.get(pk=9)
        }
    except IndexError:
        return redirect('general')
    return render_to_response("contacts/" + template, args)


def about(req):
    template = 'index.html'
    if is_eng(req):
        template = 'index_eng.html'
    args = dict()
    articles = Article.objects.only(
        'name',
        'name_en'
    ).order_by('-id')
    args['articles'] = articles[:3]
    contact = Contacts.objects.all()
    args['contacts'] = {'tel1': contact.get(pk=1), 'tel2': contact.get(pk=2), 'email': contact.get(pk=3)}
    return render_to_response("about/" + template, args)


def description(req, filter=''):
    template = 'index.html'
    if is_eng(req):
        template = 'index_eng.html'
    args = dict()
    if filter:
        try:
            item = Filter.objects.get(pk=filter)
        except Filter.DoesNotExist:
            return redirect('general')
        else:
            articles = Article.objects.only(
                'name',
                'name_en'
            ).order_by('-id')
            args['articles'] = articles[:3]
            args['filter'] = item
            args['features'] = item.feature_set.all().order_by('-id')
            contact = Contacts.objects.all()
            args['contacts'] = {'tel1': contact.get(pk=1), 'tel2': contact.get(pk=2), 'email': contact.get(pk=3)}
            return render_to_response("description/" + template, args)

    return redirect('general')


def construction(req, filter_item=''):
    template = 'index.html'
    if is_eng(req):
        template = 'index_eng.html'
    args = dict()
    if filter_item:
        args['items'] = list()
        if filter_item == '_all':
            items = Construction.objects.all().order_by('-id')
        else:
            items = Construction.objects.filter(filter_link=filter_item).order_by('-id')
        for item in items:
            args['items'].append(dict(item=item, photos=item.photo_set.all()))
        articles = Article.objects.only(
            'name',
            'name_en'
        ).order_by('-id')
        args['articles'] = articles[:3]
        contact = Contacts.objects.all()
        args['contacts'] = {'tel1': contact.get(pk=1), 'tel2': contact.get(pk=2), 'email': contact.get(pk=3)}
        return render_to_response("construct/" + template, args)
    return redirect('general')


def blog(req, page=''):
    template = 'index.html'
    if is_eng(req):
        template = 'index_eng.html'
    args = dict()
    items = Article.objects.only(
        'name',
        'name_en',
        'short_description',
        'short_description_en',
        'photo'
    ).order_by('-date')
    paginator = Paginator(items, 3)
    try:
        args['items'] = paginator.page(page)
    except PageNotAnInteger:
        args['items'] = paginator.page(1)
    except EmptyPage:
        args['items'] = paginator.page(paginator.num_pages)
    articles = Article.objects.only(
        'name',
        'name_en'
    ).order_by('-id')
    args['articles'] = articles[:3]
    contact = Contacts.objects.all()
    args['contacts'] = {'tel1': contact.get(pk=1), 'tel2': contact.get(pk=2), 'email': contact.get(pk=3)}
    return render_to_response("blog/" + template, args)


def blog_post(req, article=''):
    template = 'index.html'
    if is_eng(req):
        template = 'index_eng.html'
    args = dict()
    try:
        args['item'] = Article.objects.get(id=article)
    except Article.DoesNotExist:
        pass
    else:
        articles = Article.objects.only(
            'name',
            'name_en'
        ).order_by('-id')
        args['articles'] = articles[:3]
        contact = Contacts.objects.all()
        args['contacts'] = {'tel1': contact.get(pk=1), 'tel2': contact.get(pk=2), 'email': contact.get(pk=3)}
        return render_to_response("article/" + template, args)
    return redirect('blog')


def ajax_form(req):
    template = 'index.html'
    if is_eng(req):
        template = 'index_eng.html'
    if req.method == 'POST':
        form = MessageForm(req.POST, req.FILES)
        print(req.FILES)
        print(req.POST)
        if form.is_valid():
            form.save()
            return HttpResponse("OK")
        else:
            args = dict(csrf(req))
            args['form'] = form
            print(form.errors.items())
            if is_eng(req):
                for key, value in form.errors.items():
                    if value[0] == 'Обязательное поле.':
                        form.errors[key][0] = 'Required field.'
                    elif value[0] == 'Введите правильный адрес электронной почты.':
                        form.errors[key][0] = 'Please enter a valid email address.'
            s = get_template("form/" + template).render(args)
            return HttpResponse(s)
    else:
        args = dict()
        args.update(csrf(req))
        args['form'] = MessageForm()
        return render_to_response("form/" + template, args)


def ajax_lang(req):
    if is_eng(req):
        del req.session['eng']
    else:
        req.session['eng'] = True
    return HttpResponse('OK')
