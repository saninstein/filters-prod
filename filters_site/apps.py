from django.apps import AppConfig


class FiltersSiteConfig(AppConfig):
    name = 'filters_site'
    verbose_name = "Фильтры"
