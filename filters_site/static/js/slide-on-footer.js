/**
 * Created by Alex on 18.08.2016.
 */
$(document).ready(function () {
    $('#land-slides img:first').addClass('show-slide');
    $('#land-slides img:first').data('visible', '1');
    $.cookie('last',  $('#land-slides img:first').attr('id'));
    $('.select').mouseover(function () {
        $('.select').removeClass('selected-select');
        $(this).addClass('selected-select');
        $('#' + $.cookie('last')).removeClass('show-slide');
        var el = $("#land-slide-" + $(this).data('slide'))
        el.addClass('show-slide');
        $.cookie('last', el.attr('id'));
        $('.select span').hide();
        $($(this).find('span')).show();
       });
    });