/**
 * Created by Alex on 20.08.2016.
 */
$(document).ready(function () {
            $('#menu-open').on('click', function () {
                $('.nav').addClass('show-nav');
                $('.lang').addClass('show-nav');
                $('#menu-close').addClass('menu-close');
                $('#logo img').addClass('logo-c');
                $(this).removeClass('show-menu');
            });

             $('#menu-close').on('click', function () {
                 $('.nav').removeClass('show-nav');
                 $('.lang').removeClass('show-nav');
                 $(this).removeClass('menu-close');
                 $('#logo img').removeClass('logo-c');
                 $('#menu-open').addClass('show-menu');
            });

            $(window).on('resize', function () {
                windowResize();
            })
});


function windowResize() {
    if($(window).width() > '900' || $(window).width() > $(window).height()) {
        $('#menu-close').removeClass('menu-close');
        $('.nav').removeClass('show-nav');
        $('.lang').removeClass('show-nav');
        $('#menu-open').addClass('show-menu');
    }
}