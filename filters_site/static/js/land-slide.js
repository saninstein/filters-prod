$(document).ready(function() {
    if($(window).width() > '1200'){
        $("#full-page").fullpage({
        onLeave: function(index, nextIndex, direction) {
            var leavingSection = $(this);//after leaving section 2
            if (direction == 'down') {
                $('header').fadeOut('slow');
            } else if (direction == 'up') {
                $('header').fadeIn('slow');
            }
        },
    });
    }
});

