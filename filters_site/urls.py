from django.conf.urls import url
from filters_site import views


urlpatterns = [
    url(r'^$', views.general, name='general'),
    url(r'^about/$', views.about, name='about'),
    url(r'^contacts/$', views.contacts, name='contacts'),
    url(r'^blog/$', views.blog, name='blog'),
    url(r'^blog/(?P<page>\d+)', views.blog, name='blog_page'),
    url(r'^skills/(?P<num>\d+)', views.skill, name='skill'),
    url(r'^slide/(?P<num>\d+)', views.slide, name='slide'),
    url(r'^filter/(?P<filter>\d+)', views.description, name='filter'),
    url(r'^construct/(?P<filter_item>\d+)', views.construction, name='construct'),
    url(r'^all-constructions/$', views.construction, {'filter_item': '_all'}, name='construct_all'),
    url(r'^blog-post/(?P<article>\d+)', views.blog_post, name='article'),
    url(r'^ajax_form/$', views.ajax_form, name='form'),
    url(r'^ajax_lang/$', views.ajax_lang, name='lang'),
]
