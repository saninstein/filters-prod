from django.contrib import admin
from django.utils.html import format_html
from filters_site.models import *
from filters_site.forms import SliderForm


class PhotoInline(admin.StackedInline):
    model = Photo
    extra = 1


class FeatureInline(admin.StackedInline):
    model = Feature
    extra = 1
    fieldsets = [
        ('Характеристика', {'fields': ['key', 'value']}),
        (None, {'fields': ['key_en', 'value_en']}),
    ]


class ConstructionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['filter_link', 'name', 'description']}),
        ('Для английской версии сайта', {'fields': ['name_en', 'description_en']}),
    ]
    inlines = [PhotoInline]
    search_fields = ('name', 'description')
    list_filter = ('filter_link',)
    preserve_filters = False


class FilterAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'description', 'photo']}),
        ('Для английской версии сайта', {'fields': ['name_en', 'description_en']}),
    ]

    inlines = [FeatureInline]

    def has_add_permission(self, request):
        return False


class ArticleAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'short_description', 'description', 'photo']}),
        ('Для английской версии сайта', {'fields': ['name_en', 'short_description_en', 'description_en']}),
    ]
    list_display = ('name', 'date')
    search_fields = ('name', 'description')

    def view_on_site(self, inst):
        return inst.get_absolute_url()


class SliderAdmin(admin.ModelAdmin):
    form = SliderForm

    fieldsets = [
        (None, {'fields': ['name', 'short_description', 'description', 'photo']}),
        ('Для английской версии сайта', {'fields': ['name_en', 'short_description_en', 'description_en']}),
    ]
    list_display = ('name', 'date')

    def has_add_permission(self, request):
        return False


class MessageAdmin(admin.ModelAdmin):
    exclude = ()
    list_per_page = 10
    list_display_links = None
    search_fields = ('name', 'message', 'email', 'number')
    list_filter = ('email', 'company')
    list_display = ('id', 'company', 'email', 'name', 'number', 'date', 'file', 'url_link', 'message')
    date_hierarchy = 'date'

    def has_add_permission(self, request):
        return False

    def url_link(self, inst):
        return format_html('<a href="%s">%s</a>' % (inst.url, inst.url))

    url_link.short_description = Message._meta.get_field('url').verbose_name.title()


class ContactsAdmin(admin.ModelAdmin):
    exclude = ()

    def has_add_permission(self, request):
        return False

admin.site.register(Message, MessageAdmin)
admin.site.register(Filter, FilterAdmin)
admin.site.register(Construction, ConstructionAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Slider, SliderAdmin)
admin.site.register(Skills, SliderAdmin)
admin.site.register(Contacts, ContactsAdmin)
admin.site.site_header = 'FILTERS OIL GAS'
admin.site.site_title = 'Административный сайт'

