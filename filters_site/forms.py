from django import forms
from filters_site.models import Message, Slider


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = '__all__'


class SliderForm(forms.ModelForm):
    class Meta:
        model = Slider
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SliderForm, self).__init__(*args, **kwargs)
        self.fields['photo'].help_text = 'Изображение 750х300'
