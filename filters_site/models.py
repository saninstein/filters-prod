import random
import string
from PIL import Image
from os import remove, path
from django.core.urlresolvers import reverse
from django.db import models
from ckeditor_uploader import fields as fields_with_up
from ckeditor import fields
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from django.contrib.sitemaps import Sitemap


def get_uniq_name(instance, filename):
    new_name = ''.join(random.sample(string.ascii_lowercase, 6))
    new_name += filename[filename.find('.'):]
    return new_name


def compress_img(path_img, size):
    img = Image.open(path_img)
    if size == (420, 230):
        img.thumbnail(size, Image.ANTIALIAS)
        bg = Image.new('RGB', size, (255, 255, 255))
        bg.paste(img, ((size[0] - img.size[0]) // 2, (size[1] - img.size[1]) // 2))

    else:
        bg = img.resize(size)
    remove(path_img)
    bg.save(path_img)


class Filter(models.Model):
    name = models.CharField(max_length=300, verbose_name='Название', help_text='Название фильтра')
    name_en = models.CharField(max_length=300, verbose_name='Название для Eng версии', help_text='Название фильтра для'
                                                                                                 ' Eng версии')
    photo = models.ImageField(upload_to=get_uniq_name, verbose_name='Фотография')
    description = fields.RichTextField(config_name='default', verbose_name='Описание', default='')
    description_en = fields.RichTextField(config_name='default', verbose_name='Описание для Eng версии',
                                                   default='')

    class Meta:
        verbose_name = 'Фильтры и Информация'
        verbose_name_plural = 'Фильтры и Информация'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('filter', kwargs={'filter': self.id})


class Feature(models.Model):
    key = models.CharField(max_length=300, verbose_name='Характеристика')
    key_en = models.CharField(max_length=300, verbose_name='Характеристика для Eng версии')
    value = models.CharField(max_length=300, verbose_name='Значение')
    value_en = models.CharField(max_length=300, verbose_name='Значение для Eng версии')
    filter_link = models.ForeignKey(Filter, verbose_name='Фильтр', help_text='Выберете фильтр')

    class Meta:
        verbose_name = 'Пункт характеристик'
        verbose_name_plural = 'Характеристики'

    def __str__(self):
        return ''


class Construction(models.Model):
    name = models.CharField(max_length=300, verbose_name='Название', help_text='Названия кострукции')
    name_en = models.CharField(max_length=300, verbose_name='Название для Eng версии', help_text='Названия кострукции для Eng версии')
    description = models.TextField(max_length=1000, verbose_name='Описание')
    description_en = models.TextField(max_length=1000, verbose_name='Описание для Eng версии')
    filter_link = models.ForeignKey(Filter, verbose_name='Фильтр', help_text='Выберете фильтр', blank=True, null=True)

    class Meta:
        verbose_name = 'Конструкцию'
        verbose_name_plural = 'Конструкции'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('construct_all')


class Photo(models.Model):
    photo = models.ImageField(upload_to=get_uniq_name, verbose_name='Фото', help_text="Будет сжато до 420x230")
    construction_link = models.ForeignKey(Construction, verbose_name='Конструкция')

    class Meta:
        verbose_name = 'Фото'
        verbose_name_plural = 'Фотографии'

    def save(self, *args, **kwargs):
        super(Photo, self).save(*args, **kwargs)
        compress_img(self.photo.path, (420, 230))


class Post(models.Model):
    name = models.CharField(max_length=300, verbose_name='Название статьи', help_text='Название фильтра')
    name_en = models.CharField(max_length=300, verbose_name='Название статьи для Eng версии', help_text='Название фильтра для'
                                                                                                 ' Eng версии')
    photo = models.ImageField(upload_to=get_uniq_name, verbose_name='Фотография', help_text="Будет сжато до 420x230")
    description = fields_with_up.RichTextUploadingField(config_name='editor', verbose_name='Статья', default='', blank=True)
    description_en = fields_with_up.RichTextUploadingField(config_name='editor', verbose_name='Статья для Eng версии',
                                                   default='', blank=True)
    short_description = models.CharField(max_length=230, verbose_name="Краткое описание",
                                         help_text="Для страницы со статьями", default='', blank=True)
    short_description_en = models.CharField(max_length=230, verbose_name="Краткое описание для Eng версии",
                                         help_text="Для страницы со статьями", default='', blank=True)

    date = models.DateTimeField(verbose_name='Время', auto_now_add=True)

    class Meta:
        abstract = True


class Article(Post):

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи блога'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('article', kwargs={'article': self.id})

    def save(self, *args, **kwargs):
        super(Article, self).save(*args, **kwargs)
        compress_img(self.photo.path, (420, 230))


class Slider(Post):

    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Блок Слайдер'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(Slider, self).save(*args, **kwargs)
        compress_img(self.photo.path, (750, 300))


class Skills(Post):

    class Meta:
        verbose_name = 'Возможности'
        verbose_name_plural = 'Блок Возможности'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(Skills, self).save(*args, **kwargs)
        compress_img(self.photo.path, (420, 230))

    def get_absolute_url(self):
        return reverse('skill', kwargs={'num': self.id})


class Message(models.Model):
    name = models.CharField(max_length=500, default='', verbose_name='Имя')
    company = models.CharField(max_length=500, default='', blank=True, verbose_name='Компания')
    number = models.CharField(max_length=100, default='', blank=False, verbose_name='Телефон')
    email = models.EmailField(max_length=50, default='', verbose_name='Почта')
    message = models.TextField(max_length=3000, default='', blank=True, verbose_name='Сообщение')
    file = models.FileField(upload_to=get_uniq_name, blank=True, verbose_name='Файл')
    url = models.CharField(max_length=300, default='', blank=True, verbose_name='Страница перехода')
    date = models.DateTimeField(verbose_name='Время', auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'


class Contacts(models.Model):
    val = models.CharField(max_length=200, blank=True, verbose_name='Значение')

    def __str__(self):
        return self.val

    class Meta:
        verbose_name = 'Контакт'
        verbose_name_plural = 'Блок Контакты'


@receiver(pre_delete, sender=Photo)
def construct_photo_delete(sender, instance, **kwargs):
    instance.photo.delete(False)


@receiver(pre_delete, sender=Article)
def article_photo_delete(sender, instance, **kwargs):
    instance.photo.delete(False)


@receiver(pre_delete, sender=Message)
def message_file_delete(sender, instance, **kwargs):
    instance.file.delete(False)

# Sitemap


class FilterSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Filter.objects.all()


class ArticleSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Article.objects.all()


class ArticleSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Article.objects.all()


class SkillsSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Skills.objects.all()


class StaticSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return ['about', 'contacts', 'blog', 'construct_all']

    def location(self, item):
        return reverse(item)
