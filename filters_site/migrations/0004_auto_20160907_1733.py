# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-09-07 14:33
from __future__ import unicode_literals

import ckeditor_uploader.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('filters_site', '0003_auto_20160907_1730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='description',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', verbose_name='Статья'),
        ),
        migrations.AlterField(
            model_name='article',
            name='description_en',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', verbose_name='Статья для Eng версии'),
        ),
        migrations.AlterField(
            model_name='article',
            name='short_description',
            field=models.CharField(blank=True, default='', help_text='Для страницы со статьями', max_length=230, verbose_name='Краткое описание'),
        ),
        migrations.AlterField(
            model_name='article',
            name='short_description_en',
            field=models.CharField(blank=True, default='', help_text='Для страницы со статьями', max_length=230, verbose_name='Краткое описание для Eng версии'),
        ),
        migrations.AlterField(
            model_name='skills',
            name='description',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', verbose_name='Статья'),
        ),
        migrations.AlterField(
            model_name='skills',
            name='description_en',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', verbose_name='Статья для Eng версии'),
        ),
        migrations.AlterField(
            model_name='skills',
            name='short_description',
            field=models.CharField(blank=True, default='', help_text='Для страницы со статьями', max_length=230, verbose_name='Краткое описание'),
        ),
        migrations.AlterField(
            model_name='skills',
            name='short_description_en',
            field=models.CharField(blank=True, default='', help_text='Для страницы со статьями', max_length=230, verbose_name='Краткое описание для Eng версии'),
        ),
        migrations.AlterField(
            model_name='slider',
            name='description',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', verbose_name='Статья'),
        ),
        migrations.AlterField(
            model_name='slider',
            name='description_en',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', verbose_name='Статья для Eng версии'),
        ),
        migrations.AlterField(
            model_name='slider',
            name='short_description',
            field=models.CharField(blank=True, default='', help_text='Для страницы со статьями', max_length=230, verbose_name='Краткое описание'),
        ),
        migrations.AlterField(
            model_name='slider',
            name='short_description_en',
            field=models.CharField(blank=True, default='', help_text='Для страницы со статьями', max_length=230, verbose_name='Краткое описание для Eng версии'),
        ),
    ]
